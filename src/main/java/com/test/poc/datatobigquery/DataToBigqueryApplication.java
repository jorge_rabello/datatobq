package com.test.poc.datatobigquery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataToBigqueryApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataToBigqueryApplication.class, args);
	}

}
