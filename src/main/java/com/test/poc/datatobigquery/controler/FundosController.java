package com.test.poc.datatobigquery.controler;

import com.test.poc.datatobigquery.service.BigQueryConnector;
import com.test.poc.datatobigquery.service.DataExtractorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/fundos")
@RequiredArgsConstructor
public class FundosController {

    private final DataExtractorService service;
    private final BigQueryConnector connector;

    @GetMapping
    public ResponseEntity<?> getDadosFundo() throws InterruptedException {
        // DadosFundo dadosFundo = service.queryExecutor();
        connector.anotherTest();
        //return new ResponseEntity<>(dadosFundo, HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
