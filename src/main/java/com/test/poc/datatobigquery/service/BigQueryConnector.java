package com.test.poc.datatobigquery.service;

import com.google.api.client.util.Value;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.FieldValue;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Component
@RequiredArgsConstructor
public class BigQueryConnector {

    private final BigQuery bigQuery;

    @Value("${spring.cloud.gcp.bigquery.dataset-name}")
    private String dataSetName;

    public void anotherTest() throws InterruptedException {
        String query = "SELECT  * FROM `myreportview`;";

        QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(query).build();

// Print the results.
        for (FieldValueList row : bigQuery.query(queryConfig).iterateAll()) {
            for (FieldValue val : row) {
                System.out.printf("%s,", val.toString());
            }
            System.out.printf("\n");
        }
    }

}
