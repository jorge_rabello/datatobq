package com.test.poc.datatobigquery.service;

import com.test.poc.datatobigquery.model.DadosFundo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class DataExtractorService {

    private final JdbcTemplate jdbcTemplate;
    private static final String QUERY_ALL_FUNDOS = "SELECT  * FROM reportview";

    @Autowired
    public DataExtractorService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public DadosFundo queryExecutor() {
        DadosFundo dadosFundo = jdbcTemplate.queryForObject(QUERY_ALL_FUNDOS, DadosFundo.class);
        if (dadosFundo == null) {
            throw new RuntimeException("Falha ao conectar");
        }
        return dadosFundo;
    }
}
