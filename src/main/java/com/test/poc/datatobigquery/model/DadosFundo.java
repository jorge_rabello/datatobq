package com.test.poc.datatobigquery.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class DadosFundo implements Serializable {

    private final String codfundo;
    private final Long recebimentoId;

}
